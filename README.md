# LiveNews


## 🔴 <a href="https://eremin-livenews.netlify.app" target="_blank">Демонстрация</a>

## Description

Проект для просмотра актуальной новостной ленты с использованием Next13/Tailwind/Stepzen/Graphql

## Смотри ниже!

#### Что было использовано:

- Next13 <a href='https://nextjs.org/blog/next-13'>Перейти</a>
- Tailwind <a href='https://tailwindcss.com/'>Перейти</a>
- Stepzen <a href='https://stepzen.com/'>Перейти</a>
- Graphql <a href='https://graphql.org/'>Перейти</a>
- heroicons <a href='https://heroicons.com/'>Перейти</a>
- typescript <a href='https://www.typescriptlang.org/'>Перейти</a>
