import { scalarOptions } from 'yaml'

import Int = scalarOptions.Int

export interface IDataEntry {
	author: string | null
	category: string
	country: string
	description: string
	image: string
	language: string
	published_at: string
	source: string
	title: string
	url: string
}

interface IPagination {
	count: Int
	limit: Int
	offset: Int
	total: Int
}

export interface INewsResponse {
	pagination: IPagination
	data: IDataEntry[]
}
