export type Categories =
	| 'business'
	| 'entertainment'
	| 'general'
	| 'health'
	| 'science'
	| 'sports'
	| 'technology'
