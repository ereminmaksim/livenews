import React from 'react'

import NewList from '@/components/NewList/NewList'

import { fetchNews } from '../graphApi/fetchNews'

import styles from './SearchPage.module.scss'
import { INewsResponse } from '@/sharedtypes/newsResponse.interface'

interface ISearchPage {
	searchParams?: { term: string }
}

const SearchPage = async ({ searchParams }: ISearchPage) => {
	const news: INewsResponse | any = await fetchNews(
		'general',
		searchParams?.term,
		true
	)
	console.log(news)
	console.log(searchParams?.term)
	return (
		<div>
			<h1 className={styles.searchPageTitle}>
				Search by tag: {searchParams?.term}
			</h1>
			<NewList news={news} />
		</div>
	)
}
export default SearchPage
