import { INewsResponse } from '@/sharedtypes/newsResponse.interface'

export default function sortNews(news: INewsResponse) {
	const newsWithImages = news.data.filter((news) => news.image !== null)
	const newsWithoutImages = news.data.filter((news) => news.image === null)

	const sortNewsResponse = {
		pagination: news.pagination,
		data: [...newsWithImages, ...newsWithoutImages],
	}

	return sortNewsResponse
}
