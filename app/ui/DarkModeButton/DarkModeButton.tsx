'use client'

import { MoonIcon, SunIcon } from '@heroicons/react/24/solid'
import { useTheme } from 'next-themes'
import { useEffect, useState } from 'react'

import styles from './DarkModeButton.module.scss'

const DarkModeButton = () => {
	const [mounted, setMounted] = useState(false)
	const { setTheme, theme, systemTheme } = useTheme()
	const currentTheme = theme === 'system' ? systemTheme : theme

	useEffect(() => {
		setMounted(true), []
	})

	if (!mounted) {
		return null
	}

	const onClickThemeLightHandler = () => {
		setTheme('light')
	}
	const onClickThemeDarkHandler = () => {
		setTheme('dark')
	}

	return (
		<div>
			{currentTheme === 'dark' ? (
				<SunIcon
					onClick={onClickThemeLightHandler}
					className={styles.sunTheme}
				/>
			) : (
				<MoonIcon
					onClick={onClickThemeDarkHandler}
					className={styles.darkTheme}
				/>
			)}
		</div>
	)
}
export default DarkModeButton
