'use client'

import React, { FC } from 'react'
import TimeAgo from 'react-timeago'

interface ILiveTimeAgo {
	time: string
}

const LiveTimeAgo: FC<ILiveTimeAgo> = ({ time }) => {
	return <TimeAgo date={time} />
}
export default LiveTimeAgo
