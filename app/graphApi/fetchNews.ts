import { gql } from 'graphql-request'
import * as process from 'process'

import sortNews from '@/utils/sortNews'

import { Categories } from '@/sharedtypes/category.type'

export const fetchNews = async (
	category: Categories | string,
	keywords?: string,
	isDynamic?: boolean
) => {
	const query = gql`
		query MyQuery(
			$access_key: String!
			$categories: String!
			$keywords: String
		) {
			myQuery(
				access_key: $access_key
				categories: $categories
				countries: "gb"
				sort: "published_desc"
				keywords: $keywords
			) {
				data {
					author
					category
					country
					description
					image
					language
					published_at
					source
					title
					url
				}
				pagination {
					count
					limit
					offset
					total
				}
			}
		}
	`
	const result = await fetch(
		// 'https://kavaje.stepzen.net/api/punk-wombat/__graphql ',
		'https://essex.stepzen.net/api/killer-duck/__graphql',
		{
			method: 'POST',
			cache: isDynamic ? 'no-cache' : 'default',
			next: isDynamic ? { revalidate: 0 } : { revalidate: 20 },
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Apikey ${process.env.STEPZEN_API_KEY}`,
			},
			body: JSON.stringify({
				query,
				variables: {
					access_key: process.env.MEDIASTACK_API_KEY,
					categories: category,
					keywords: keywords,
				},
			}),
		}
	)
	console.log('LOADING NEW DATA...', category, keywords)

	const newResponse = await result.json()

	const news = sortNews(newResponse.data.myQuery)

	return news
}
