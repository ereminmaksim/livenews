import { categories } from '@config/constants'
import React from 'react'

import NewList from '@/components/NewList/NewList'

import { fetchNews } from './graphApi/fetchNews'
import { INewsResponse } from '@/sharedtypes/newsResponse.interface'

const HomePage = async () => {
	const news: INewsResponse = await fetchNews(categories.join(','))
	// await new Promise((resolve) => setTimeout(resolve, 3000))

	return (
		<div>
			<NewList news={news} />
		</div>
	)
}
export default HomePage
