import { notFound } from 'next/navigation'
import React, { FC } from 'react'

import article from '@/components/NewList/Article/Article'

import LiveTimeAgo from '../ui/TimeAgo/DateTime'

import styles from './ArticlePage.module.scss'
import { IDataEntry } from '@/sharedtypes/newsResponse.interface'

const ArticlePage: FC<{ searchParams?: IDataEntry }> = ({ searchParams }) => {
	if (
		(searchParams && Object.entries(searchParams).length === 0) ||
		!searchParams
	) {
		return notFound()
	}
	const data: IDataEntry = searchParams

	return (
		<article className={styles.articlePageWrapper}>
			<section className={styles.articlePageSection}>
				{data?.image && (
					<img
						className={styles.articlePage__img}
						src={data.image}
						alt={data.title}
					/>
				)}
				<div className={styles.articlePageAuthorWrapper}>
					<h1 className={styles.articlePageTitle}>{data.title}</h1>
					<div className={styles.articlePageSubscribeWrapper}>
						<h2 className={styles.articlePageAuthor}>
							Author: {data.author === null ? data.author : 'none'}
						</h2>
						<h2 className={styles.articlePageSource}>Source: {data.source}</h2>
						<p className={styles.articlePagePublished}>
							<LiveTimeAgo time={data.published_at} />
						</p>
					</div>
					<p className={styles.articlePageDescription}>{data.description}</p>
				</div>
			</section>
		</article>
	)
}
export default ArticlePage
