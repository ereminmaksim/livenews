'use client'

import { ThemeProvider } from 'next-themes'
import React, { ReactNode } from 'react'

const Providers = ({ children }: { children: ReactNode }) => {
	return (
		<ThemeProvider enableSystem={true} attribute="class">
			{children}
		</ThemeProvider>
	)
}
export default Providers
