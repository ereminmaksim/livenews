import { StaticImageData } from 'next/image'
import { ReactNode } from 'react'

import './assets/styles/globals.scss'
import Header from './components/Header/Header'
import Providers from './providers/Providers'

export default function RootLayout({ children }: { children: ReactNode }) {
	return (
		<html>
			<head />
			<body className="bg-gray-300 dark:bg-zincs transition-all duration-700">
				<Providers>
					<Header />
					<div className="wrapper">{children}</div>
				</Providers>
			</body>
		</html>
	)
}
