import { categories } from '@config/constants'
import React from 'react'

import NewList from '@/components/NewList/NewList'

import { fetchNews } from '../../graphApi/fetchNews'

import styles from './NewsPage.module.scss'
import { Categories } from '@/sharedtypes/category.type'
import { INewsResponse } from '@/sharedtypes/newsResponse.interface'

type Props = {
	params: { category: Categories }
}

async function NewsCategory({ params: { category } }: Props) {
	const news: INewsResponse = await fetchNews(category)

	return (
		<div>
			<h1 className={styles.newsPageTitle}>{category}</h1>
			<NewList news={news} />
		</div>
	)
}

export default NewsCategory

// Static preload prebuild

export async function generateStaticParams() {
	return categories.map((category) => ({
		category: category,
	}))
}
