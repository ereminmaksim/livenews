'use client'

import { useRouter } from 'next/navigation'
import React, { FC } from 'react'

import styles from './ReadMoreButton.module.scss'
import { IDataEntry } from '@/sharedtypes/newsResponse.interface'

const ReadMoreButton: FC<{ data: IDataEntry }> = ({ data }) => {
	const router = useRouter()

	const onClickHandler = () => {
		const queryString = Object.entries(data)
			.map(([key, value]) => `${key}=${value}`.replace(/&/g, ''))
			.join('&')

		const finalQueryString = queryString.replace(/#/g, '')

		const url = `/article?${finalQueryString}`

		router.push(url)
	}

	return (
		<button onClick={onClickHandler} className={styles.buttonClick}>
			Read More
		</button>
	)
}
export default ReadMoreButton
