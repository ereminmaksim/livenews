import React, { FC } from 'react'

import ReadMoreButton from '@/components/NewList/ReadMoreButton/ReadMoreButton'

import LiveTimeAgo from 'ui/TimeAgo/DateTime'

import styles from './Article.module.scss'
import { IDataEntry } from '@/sharedtypes/newsResponse.interface'

const Article: FC<{ data: IDataEntry }> = ({ data }) => {
	return (
		<article className={styles.wrapper}>
			{data.image && (
				<img
					className={styles.article__img}
					src={data.image}
					alt={data.title}
				/>
			)}
			<div className={styles.wrap_typography}>
				<div className={styles.wrap_typography__heading}>
					<h2 className={styles.wrap_typography__heading__title}>
						{data.title}
					</h2>
					<section className={styles.section_typography}>
						<p>{data.description}</p>
					</section>

					<footer className={styles.footer}>
						<p>{data.source}</p>
						<p>
							<LiveTimeAgo time={data.published_at} />
						</p>
					</footer>
				</div>
				<ReadMoreButton data={data} />
			</div>
		</article>
	)
}
export default Article
