import React, { FC } from 'react'

import Article from '@/components/NewList/Article/Article'

import styles from './NewList.module.scss'
import { INewsResponse } from '@/sharedtypes/newsResponse.interface'

interface INewList {
	news: INewsResponse
}

const NewList: FC<INewList> = ({ news }) => {
	return (
		<main className={styles.wrapper}>
			{news?.data?.map((item) => (
				<Article key={item.title} data={item} />
			))}
		</main>
	)
}
export default NewList
