import Image from 'next/image'
import Link from 'next/link'
import React, { FC } from 'react'

import SearchBox from '@/components/Header/SearchBox/SearchBox'

import liveNews from '../../../public/liveNews.jpg'
import DarkModeButton from '../../ui/DarkModeButton/DarkModeButton'

import styles from './Header.module.scss'
import NavLinks from './Navlinks/Navlinks'

const Header = () => {
	return (
		<header>
			<div className={styles.headerWrap}>
				<Link href="/" prefetch={false}>
					<Image src={liveNews} alt="LiveNews" className={styles.headerIcon} />
				</Link>

				<Link href="/" prefetch={false}>
					<h1 className={styles.headerWrap__heading}>
						Eremin{' '}
						<span className={styles.headerWrap__subtitle}>Developer</span> News
					</h1>
				</Link>

				<span className={styles.darkMode}>
					<DarkModeButton />
				</span>
			</div>

			<NavLinks />

			<SearchBox />
		</header>
	)
}
export default Header
