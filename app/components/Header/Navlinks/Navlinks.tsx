'use client'

import { categories } from '@config/constants'
import { usePathname } from 'next/navigation'
import React from 'react'

import NavLink from '@/components/Header/Navlinks/Navlink/Navlink'

import styles from './Navlinks.module.scss'

const NavLinks = () => {
	const pathName = usePathname()

	const isActive = (path: string) => {
		return pathName?.split('/').pop() === path
	}
	/**
	 * //mysite.com/user/technology [mysite.com, user, technology]
	 */

	return (
		<nav className={styles.wrapper}>
			{categories.map((category) => (
				<NavLink
					key={category}
					category={category}
					isActive={isActive(category)}
				/>
			))}
		</nav>
	)
}
export default NavLinks
