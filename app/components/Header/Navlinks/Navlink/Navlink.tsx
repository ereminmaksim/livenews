import Link from 'next/link'
import React, { FC } from 'react'

import styles from './NavLink.module.scss'

interface INavLink {
	category: string
	isActive: boolean
}

const NavLink: FC<INavLink> = ({ category, isActive }) => {
	return (
		<Link
			className={`${styles.navLink} ${
				isActive &&
				`underline decoration-oranges underline-offset-4 font-bold text-lg`
			}`}
			href={`/news/${category}`}
		>
			{category}
		</Link>
	)
}
export default NavLink
