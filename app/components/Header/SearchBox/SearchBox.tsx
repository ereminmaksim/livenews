'use client'

import { useRouter } from 'next/navigation'
import React, { ChangeEvent, FormEvent, useState } from 'react'

import styles from './SearchBox.module.scss'

const SearchBox = () => {
	const [input, setInput] = useState('')
	const router = useRouter()
	const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
		setInput(e.target.value)
	}

	const onSearchHandler = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		if (!input) return
		router.push(`/search?term=${input}`)
	}

	return (
		<form onSubmit={onSearchHandler} className={styles.formWrapper}>
			<input
				value={input}
				onChange={onChangeHandler}
				placeholder="Search Keywords..."
				className={styles.formInput}
				type="text"
			/>
			<button className={styles.formBtn} disabled={!input} type="submit">
				Search
			</button>
		</form>
	)
}
export default SearchBox
