import { Categories } from '@/sharedtypes/category.type'

export const categories: Categories[] = [
	'business',
	'entertainment',
	'general',
	'health',
	'science',
	'sports',
	'technology',
]
